﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {
	public GameObject playButton;
	public Camera camera;
	public LayerMask layer;
	public GameObject[] cups;
	public GameObject ball;

	public void Play() {
		StartCoroutine(Shuffle());
	}

	IEnumerator Shuffle() {
		yield return Lift(cups[1]);

		for (int n = 0; n < 15; n = n + 1) {
			int i = Random.Range(0, 2);
			int offset = Random.Range(1, 2);
			int j = (i + offset) % 3;

			yield return Swap(cups[i], cups[j]);
		}
	}

	IEnumerator Swap(GameObject a, GameObject b) {
		Vector3 wasAtA = a.transform.position;
		Vector3 wasAtB = b.transform.position;
		Vector3 nowAtA = wasAtA;
		Vector3 nowAtB = wasAtB;

		float startTime = Time.time;
		float targetTime = 0.25f;
		float ratio = 0;

		float maxZ = 2.0f;
		float acceleration = -4.0f * maxZ;
		float initialVelocityZ = -acceleration;

		do {
			float elapsedTime = Time.time - startTime;
			ratio = elapsedTime / targetTime; 
			ratio = Mathf.Clamp01(ratio);
			nowAtA.x = Mathf.Lerp(wasAtA.x,wasAtB.x,ratio);
			nowAtB.x = Mathf.Lerp(wasAtB.x,wasAtA.x,ratio);
			nowAtA.z= acceleration*ratio*ratio + initialVelocityZ*ratio;
			nowAtB.z = -nowAtA.z;
			a.transform.position = nowAtA;
			b.transform.position = nowAtB;
			yield return null;
		} while (ratio < 1);

	}

	void Update(){
		if (Input.GetMouseButtonDown(0)) {
			Ray ray = camera.ScreenPointToRay(Input.mousePosition);
			Debug.Log(ray);

			RaycastHit hit;
			if (Physics.Raycast(ray, out hit, Mathf.Infinity, layer)) {
				StartCoroutine(Lift(hit.collider.gameObject));
			}
		}
	}

	IEnumerator Lift(GameObject cup){
		ball.transform.parent = null;

		// Ascent
		float startTime = Time.time;
		float targetTime = 0.5f;
		Vector3 startPosition = cup.transform.position;
		Vector3 endPosition = startPosition + 2 * Vector3.up;
		float ratio = 0;

		do {
			float elapsedTime = Time.time - startTime;
			ratio = elapsedTime / targetTime; 
			cup.transform.position = Vector3.Lerp(startPosition, endPosition, ratio);
			yield return null;
		} while (ratio < 1);

		yield return new WaitForSeconds(2);

		// Descent
		startTime = Time.time;
		ratio = 0;

		do {
			float elapsedTime = Time.time - startTime;
			ratio = elapsedTime / targetTime; 
			cup.transform.position = Vector3.Lerp(endPosition, startPosition, ratio);
			yield return null;
		} while (ratio < 1);

		ball.transform.parent = cups[1].transform;
	}
}
